Jason Yang Lab 1

Linker Lab (Lab 1) for Operating Systems Spring 2016
1 source code file(s) are part of this submission:
        -> linker.cpp


To compile the lab, execute:
$ g++ -std=gnu++0x linker.cpp

To run the lab, execute:
$ ./a.out
<paste content>
Ctrl-D

The code was tested on mauler.cims.nyu.edu.


3000 character limit
C++

