#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <regex>
#include <map>
#include <set>
#include <algorithm>
#include <sstream>
//Jason Yang OS lab 1
using namespace std;
//prototype and global 
vector<int> eraseind;
int custommap(string v1, vector<string> keyv, vector<int> valuev);
stringstream streamm;
int main() {
	string flines;

	string total;
	string filename;
	string input;
	char scn[3000];
	
//	while (true) {
//		if (scanf("%s", &scn) != EOF){
//		break;
//}
//	}
	
	//ifstream file(filename);
	//if (file.is_open()) {
	
	while (getline(cin, flines)) {
		if (flines.find('\n')) {
			flines += " ";
		}

		total += flines;
	}

	//for (int i = 0; i < 3000; i++) {
	//	if (scn[i] == '\n') {
	//		scn[i]= ' ';
	//	}
	//	total += scn;
	//}

	//}
	//file.close();

	//else { cout << "failed"; }



	//a lot of flags in order to transverse file info and place elements in the correct vectors
	
	int level = 0; // 1 2 or 3 passes
	int flagspace = 1;
	int settingkey = 0;

	int vpointing; 
	string pointing; //acts like buffer
	int pushlevel = 0;
	int settingvalue = 0;

	int counter = -1; //for relative address 
	int offset = 0; //offset of modules

	vector <string> addrs;
	

	map<int, int> maxl3sizetomod; //when definition exceeds size of module
	map <string, int> l1maptomod; // for error printing
	vector <string> oldl1keys;
	vector<int> oldl1values;
	vector<int> oldrel1values; // for  error
	vector<int> oldrel1offsets; // for error stuff

	
	vector <string> l3keys;
	vector <int> l3values;


	vector <vector<string>> mod2key;
	vector<string> l2keybuf;
	vector <vector<int>> mod2value;
	vector <int> l2valuebuf;

	vector <vector<string>> mod3key;
	vector<string> l3keybuf;
	vector <vector<int>> mod3value;
	vector <int> l3valuebuf;
	
	//1st pass - use buffers or vectors which will grow then be placed in another vector 
	// takes advantage of space to signal which to push the data from the buffer onto the vector
	//logic when transversing a space depends on the flags 
	//level is the current line# on each module
	for (int i = 0; i < total.length(); i++) { 

		if (flagspace == 1 && isdigit(total[i]) && settingkey == 0) {
			if ((level % 3) + 1 == 3 && level == 2) {
				mod2key.push_back(l2keybuf); //switching from 2 to 3 push on 
				l2keybuf.clear();
				mod2value.push_back(l2valuebuf);
				l2valuebuf.clear();
			}
			if (((level % 3) + 1 == 1 && level == 3)  ) {
				mod3key.push_back(l3keybuf); //switching from 3 to 1
				l3keybuf.clear();
				mod3value.push_back(l3valuebuf);
				l3valuebuf.clear();
			}


			level = (level % 3) + 1;
			flagspace = 0;
			pointing = total[i];
			pushlevel = 1;
			//

			counter++;
			
		}
		else if (isdigit(total[i]) && flagspace != 1) {
			//append descriptive # to level
			pointing = pointing + total[i];
		}
		else if (total[i] == ' ' || total.at(i) == '\n') {
				flagspace = 1;
				if (pushlevel == 1) {
					//puush level address on to array
					if (level == 1 && counter != 0) {
						offset += stoi(addrs[counter - 1]);
						//cout << offset << " ";
					}
					
					//cout << level << " " << offset << '\n';
					addrs.push_back(pointing);
					
					pushlevel = 0;
					pointing = ""; //clear pointing variable
				}
				if (settingvalue == 1) {
					vpointing = stoi(pointing);
					if (level == 1) {
						oldrel1values.push_back(vpointing); //relative for error processing
						oldrel1offsets.push_back(offset);
						vpointing = stoi(pointing) + offset; //set offset of variable 1st pass
						oldl1values.push_back(vpointing);
						
						//	cout << vpointing << " ";
					}
					else if (level == 3) {
						if (l3keys.back() == "R") {
							vpointing = vpointing + offset; //set offset of variable 1st pass

						}
						l3values.push_back(vpointing);
						l3valuebuf.push_back(vpointing);


					}
					else if (level == 2) {
						l2valuebuf.push_back(vpointing);
					}
					

					//push value on to array
					if (i != total.length() - 1) {
						settingvalue = 0;
					}
					else {
						vpointing = stoi(pointing);
						if (l3keys.back() == "R") {
							vpointing = vpointing + offset; //set offset of variable 1st pass

						}
			
						l3values.push_back(vpointing); // special case on last value
						mod3key.push_back(l3keybuf); //pushing on last line
						l3keybuf.clear();

						l3valuebuf.push_back(vpointing);
					
						mod3value.push_back(l3valuebuf);
						l3valuebuf.clear();

					}
					settingkey = 0;
					pointing = "";
					vpointing = 0;
				}

			}
			else if (flagspace == 1 && isdigit(total[i]) && settingkey == 1) {
				//value set to key
				settingvalue = 1;
				flagspace = 0;
				if (level == 1) {
					oldl1keys.push_back(pointing);
					l1maptomod[pointing] = (counter / 3); //  counter/3 is the module #
					
				}
				if (level == 3) {
					l3keys.push_back(pointing);
					l3keybuf.push_back(pointing);
					maxl3sizetomod[(counter / 3)] = stoi(addrs[counter]); //for error processing get the max size of ea module
					
				}
				if (level == 2) {
					l2keybuf.push_back(pointing);
				}
			
				pointing = total[i];
			}
			else if (flagspace == 1 && isalpha(total[i])) {
				//key set
				settingkey = 1;
				flagspace = 0;
				pointing = total[i];

			}
			else if (flagspace != 1 && isalpha(total[i])) {
				//append letter to letter
				pointing = pointing + total[i];
			}



		}

		set<string> usedlist;


//prune duplicates
	
		
		cout << "\nSymbol Table\n";
		
		map<string, string> l1maperror;
		vector<string> l1keys;
		vector<int> l1values;
		vector<int> rel1values;
		vector<int> rel1offsets;
		for (int i = 0; i < oldl1keys.size(); i++) {
			if (find(l1keys.begin(), l1keys.end(), oldl1keys[i]) == l1keys.end()) {
				l1keys.push_back(oldl1keys[i]);
				l1values.push_back(oldl1values[i]);
				rel1values.push_back(oldrel1values[i]);
				rel1offsets.push_back(oldrel1offsets[i]);
			}
			else {
				l1maperror[oldl1keys[i]] = "Error: This variable is multiply defined; first value used.";
				
			}
		}
		
		for (int i = 0; i < l1keys.size(); i++) {
			if (maxl3sizetomod[l1maptomod[l1keys[i]]] <= rel1values[i]) {
				l1values[i] = rel1offsets[i]; // bc turn to 0
				streamm << l1maptomod[l1keys[i]];
				l1maperror[l1keys[i]] = "Error: The value of "+l1keys[i] +" is outside module " + streamm.str() + "; zero (relative) used.";
				streamm.str("");
			}
			cout << l1keys[i] << "=" << l1values[i] << " " << l1maperror[l1keys[i]] << '\n';
		}


///second pass mostly modifying the third line of each module
		vector<vector<string>> mod3keyerror = mod3key;
		vector<vector<int>> mod3valueerror = mod3value;
		int propagaterror = 0;
		map<pair<int,int>, string> l3toerror; //maping unique locations to error
		for (int i = 0; i < mod3value.size(); i++) {
			for (int a = 0; a < mod2key[i].size(); a++) {
				
				int current = mod2value[i][a];
				if (mod3key[i][current] != "E") { //NEED THIS FOR  TREATED AS E TYPE
				//	cout << mod3key[i][current] << " treated as E type \n";
					l3toerror[make_pair(i,current)] = "Error: " + mod3key[i][current] + " type address on use chain; treated as E type.";
					mod3keyerror[i][current] = " ";
					mod3valueerror[i][current] = -1;
						
					
				} 
				
			//	cout << mod3value[i][current];
				while (true) {
					if ((mod3value[i][current] % 1000) > mod3value[i].size() && ((mod3value[i][current] % 1000) != 777)) {
							//cout << "chain\n" << mod3value[i][current];
							l3toerror[make_pair(i, current)] = "Error: Pointer in use chain exceeds module size; chain terminated.";
							mod3value[i][current] = (((mod3value[i][current]) / 1000) * 1000) + custommap(mod2key[i][a],l1keys,l1values);
					
							mod3keyerror[i][current] = " ";
							mod3valueerror[i][current] = -1;
							
							
							break;
						}
					if (((mod3value[i][current] % 1000) == 777) ) {
						
						//cout << mod3key[i][current];
						if (find(l1keys.begin(), l1keys.end(), mod2key[i][a]) == l1keys.end()) {
					//	if (l1map.count(mod2key[i][a]) == 0) {
							mod3value[i][current] = (((mod3value[i][current]) / 1000) * 1000); // not found boi
							
						//	cout << mod2key[i][a] << " is not defined; zero used";
							l3toerror[make_pair(i,current)] = mod2key[i][a] + " is not defined; zero used";
							
							mod3keyerror[i][current] = " ";
							mod3valueerror[i][current] = -1;
							
							
							propagaterror = 1;
						}
						else {
							
							
							mod3value[i][current] = (((mod3value[i][current]) / 1000) * 1000) + custommap(mod2key[i][a],l1keys,l1values);
							

							if (mod3key[i][current] != "E") {
							//	cout << mod3key[i][current] << " treated as E type \n";
								l3toerror[make_pair(i, current)] = "Error: " + mod3key[i][current] + " type address on use chain; treated as E type.";
							}
							
							mod3keyerror[i][current] = " ";
							mod3valueerror[i][current] = -1;
							
						}

						usedlist.insert(mod2key[i][a]); // used
						propagaterror = 0;
						break;
					} // end 777 check
				
						int temp = mod3value[i][current];
						//if (l1map.count(mod2key[i][a]) == 0) {
						if(find(l1keys.begin(),l1keys.end(), mod2key[i][a]) == l1keys.end()){
							mod3value[i][current] = (((mod3value[i][current]) / 1000) * 1000); // not found boi
						//	cout << mod2key[i][a] << " is not defined; zero used\n";
							l3toerror[make_pair(i,current)] = mod2key[i][a] + " is not defined; zero used";
							
							
							mod3keyerror[i][current] = " ";
							mod3valueerror[i][current] = -1;
							
							propagaterror = 1;
						}
						else {
							
							mod3value[i][current] = (((mod3value[i][current]) / 1000) * 1000) + custommap(mod2key[i][a],l1keys,l1values);

							
								mod3keyerror[i][current] = " ";
								mod3valueerror[i][current] = -1;
							
						}
						usedlist.insert(mod2key[i][a]); // used
					//	cout << mod3value[i][current] << "\n";
					//	cout << l1map[mod2key[i][a]] << "\n";
					
						current = temp % 1000;
						if (mod3key[i][current] != "E") {
							//cout << mod3key[i][current] << " treated as E type \n";
							l3toerror[make_pair(i, current)] = "Error: " + mod3key[i][current] + " type address on use chain; treated as E type.";

						}
					}
				
			}
		}
		cout << "\nMemory Map\n";
		

///output solutions


		int fcount = 0;
for (int i = 0; i < mod3key.size(); i ++) {
	for (int a = 0; a < mod3key[i].size(); a++) {
		if ((mod3key[i][a] == mod3keyerror[i][a]) && mod3keyerror[i][a] == "E") {
			cout << fcount << ":  " << mod3value[i][a] << " Error: E type address not on use chain; treated as I type.\n";
		}
		else {
		//	cout << l3toerror[make_pair(i, a)];
			cout << fcount << ":  " << mod3value[i][a] << " " << l3toerror[make_pair(i, a)] << "\n";
		}
		fcount++;
	}
}

cout << '\n';
for (int i = 0; i < l1keys.size(); i++) {
	if (find(usedlist.begin(), usedlist.end(), l1keys[i]) == usedlist.end()) {
		streamm << l1maptomod[l1keys[i]];
		cout << "Warning: " + l1keys[i] + " was defined in module " + streamm.str() + " but never used.\n";
		streamm.str("");
	}
}




}
//function acts as hashmap but takes advantage of vector pushback mechanism to get first occurrences
int custommap(string v1, vector<string> keyv, vector<int> valuev) {
	for (int i = 0; i < keyv.size(); i++) {
		if (keyv[i] == v1) {
			return valuev[i];
		}
	}
		
	//l1keys[i]] = l1values[i]
	return 0;
}

